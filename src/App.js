import React, { Component, Suspense } from 'react';
import './style.css';
import LoginPage from './Layout/LoginPage';
import NavigasiBar from './Layout/NavigasiBar';
import Layout from './Layout/index';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;
class App extends Component {
  render() {
    return (
      <Suspense fallback={loading()}>
      {/* <Layout /> */}
      <LoginPage />
      </Suspense>
    );
  }
}

export default App;
