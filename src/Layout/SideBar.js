import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import SideNav, { NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import Breadcrumbs from '@trendmicro/react-breadcrumbs';
import '@trendmicro/react-breadcrumbs/dist/react-breadcrumbs.css';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import Add from '../views/SDMAllocation/AddSDMSkillAllocation';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import ensureArray from 'ensure-array';
import List from '../views/SDM/List';
import Historis from '../views/SDM/Historis';
import Psycology from '../views/SDM/Psycology';
import Dashboard from '../views/Dashboard';
import ListSkill from '../views/SDMAllocation/ListSkill';
import ListCategory from '../views/SDMAllocation/ListCategory';
import ListSDMSkillAllocation from '../views/SDMAllocation/ListSDMSkillAllocation';
import MultiFilter from '../views/SDMAllocation/MultiFilteringSDM';
import InputProject from '../views/ProjectAssignment/InputProjectSDM';
import ListProject from '../views/ProjectAssignment/ListDetailProjectSDM';
import ListClient from '../views/ProjectAssignment/ListDetailClient';
import Hiring from '../views/ProjectAssignment/HiringSDM';
import Assignment from '../views/ProjectAssignment/AssignmentSDM';
import UpdatePsycology from '../views/SDM/UpdatePsycology';
import Detail from '../views/SDMAllocation/DetailSDMSkillAllocation';
class SideBar extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            selected: 'dashboard',
            expanded: false
        })
    }
    pageTitle = {
        'dashboard': 'Dashboard',
        'sdm/list': ['SDM', 'List SDM'],
        'sdm/historis': ['SDM', 'Historis SDM'],
        'sdm/psycology': ['SDM', 'List Psycology SDM'],
        'sdm/p/update': ['SDM', 'List Psycology SDM Update'],
        'sdmallocation/skill': ['SDM Allocation', 'List Skill'],
        'sdmallocation/category': ['SDM Allocation', 'List Category'],
        'sdmallocation/sdmskill': ['SDM Allocation', 'List SDM Skill Allocation'],
        'sdmallocation/multifilter': ['SDM Allocation', 'Multi Filtering SDM'],
        'projectassignment/input': ['Project Assignment', 'Input Project SDM'],
        'projectassignment/detail': ['Project Assignment', 'List Detail SDM'],
        'projectassignment/detailclient': ['Project Assignment', 'List Detail Client'],
        'projectassignment/hiring': ['Project Assignment', 'Hiring SDM'],
        'projectassignment/assigment': ['Project Assignment', 'Assignment SDM']
    }
    onSelect = (selected) => {
        this.setState({ selected: selected });
    };
    onToggle = (expanded) => {
        this.setState({ expanded: expanded });
    };
    renderBreadcrumbs() {
        const { selected } = this.state;
        const list = ensureArray(this.pageTitle[selected]);
        return (
            <Breadcrumbs>
                {list.map((item, index) => (
                    <Breadcrumbs.Item
                        active={index === list.length - 1}
                        key={`${selected}_${index}`}
                    >
                        {item}
                    </Breadcrumbs.Item>
                ))}
            </Breadcrumbs>
        );
    }

    render() {
        const { expanded, selected } = this.state;
        return (
            <div className="side-bar">
                <BrowserRouter>
                    <Route render={({ location, history }) => (
                        <React.Fragment>
                            <SideNav onToggle={this.onToggle} onSelect={(selected) => {
                                    this.setState({ selected: selected });
                                    const to = '/' + selected;
                                    if (location.pathname !== to) {
                                        history.push(to);
                                    }
                                }} 
                            >
                                <SideNav.Toggle />
                                <SideNav.Nav selected={selected}>
                                    <NavItem eventKey="dashboard">
                                        <NavIcon>
                                            <i className="fa fa-fw fa-home" style={{ fontSize: '1.75em' }} />
                                        </NavIcon>
                                        <NavText>Home</NavText>
                                    </NavItem>
                                    <NavItem eventKey="sdm">
                                        <NavIcon>
                                            <i className="fa fa-fw fa-group" style={{ fontSize: '1.5em', verticalAlign: 'middle' }} />
                                        </NavIcon>
                                        <NavText style={{ paddingRight: 32 }} title="SDM">
                                            SDM
                                        </NavText>
                                        <NavItem eventKey="sdm/list">
                                            <NavText>List SDM</NavText>
                                        </NavItem>
                                        <NavItem eventKey="sdm/historis">
                                            <NavText>Historis SDM</NavText>
                                        </NavItem>
                                        <NavItem eventKey="sdm/psycology">
                                            <NavText>List Psycology</NavText>
                                        </NavItem>
                                    </NavItem>
                                    <NavItem eventKey="sdmallocation">
                                        <NavIcon>
                                            <i className="fa fa-random" style={{ fontSize: '1.5em', verticalAlign: 'middle' }} />
                                        </NavIcon>
                                        <NavText style={{ paddingRight: 32 }} title="sdmallocation">
                                            SDM Allocation
                                        </NavText>
                                        <NavItem eventKey="sdmallocation/skill">
                                            <NavText>List Skill</NavText>
                                        </NavItem>
                                        <NavItem eventKey="sdmallocation/category">
                                            <NavText>List Category</NavText>
                                        </NavItem>
                                        <NavItem eventKey="sdmallocation/sdmskill">
                                            <NavText>List SDM Skill Allocation</NavText>
                                        </NavItem>
                                        <NavItem eventKey="sdmallocation/multifilter">
                                            <NavText>Multi-filtering SDM</NavText>
                                        </NavItem>
                                    </NavItem>
                                    <NavItem eventKey="projectassignment">
                                        <NavIcon>
                                            <i className="fa fa-tasks" style={{ fontSize: '1.5em', verticalAlign: 'middle' }} />
                                        </NavIcon>
                                        <NavText style={{ paddingRight: 32 }} title="projectassignment">
                                            Project Assignment
                                        </NavText>
                                        <NavItem eventKey="projectassignment/input">
                                            <NavText>Input Project SDM</NavText>
                                        </NavItem>
                                        <NavItem eventKey="projectassignment/detail">
                                            <NavText>List Detail Project SDM</NavText>
                                        </NavItem>
                                        <NavItem eventKey="projectassignment/detailclient">
                                            <NavText>List Detail Client</NavText>
                                        </NavItem>
                                        <NavItem eventKey="projectassignment/hiring">
                                            <NavText>Hiring SDM</NavText>
                                        </NavItem>
                                        <NavItem eventKey="projectassignment/assigment">
                                            <NavText>SDM Assignment</NavText>
                                        </NavItem>
                                    </NavItem>
                                </SideNav.Nav>
                            </SideNav>
                            <div
                                style={{
                                    marginLeft: expanded ? 240 : 64,
                                    padding: '15px 20px 0 20px'
                                }}
                            >
                                <main
                                    style={{
                                        width: '100%'
                                    }}
                                >
                                    {this.renderBreadcrumbs()}
                                    <Switch>
                                        <Route path="/dashboard" exact component={props => <Dashboard />} />
                                        <Route path="/sdm/list" component={props => <List />} />
                                        <Route path="/sdm/historis" component={props => <Historis />} />
                                        <Route path="/sdm/psycology" component={props => <Psycology />} />
                                        <Route path="/sdm/p/update" component={props => <UpdatePsycology />} />
                                        <Route path="/sdmallocation/skill" component={props => <ListSkill />} />
                                        <Route path="/sdmallocation/category" component={props => <ListCategory /> } />
                                        <Route path="/sdmallocation/sdmskill" component={props => <ListSDMSkillAllocation />} />
                                        <Route path="/sdmallocation/ss/add" component={props => <Add />} />
                                        <Route path="/sdmallocation/ss/detail" component={props => <Detail />} />
                                        <Route path="/sdmallocation/multifilter" component={props => <MultiFilter /> } />
                                        <Route path="/projectassignment/input" component={props => <InputProject /> } />
                                        <Route path="/projectassignment/detail" component={props => <ListProject /> } />
                                        <Route path="/projectassignment/detailclient" component={props => <ListClient /> } />
                                        <Route path="/projectassignment/hiring" component={props => <Hiring /> } />
                                        <Route path="/projectassignment/assigment" component={props => <Assignment /> } />
                                    </Switch>
                                </main>
                            </div>
                        </React.Fragment>
                    )}
                    />
                </BrowserRouter>
            </div>
        );
    }
}

export default SideBar;