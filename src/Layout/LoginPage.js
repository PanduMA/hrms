import React, { Component } from 'react';
import {Redirect, history,Switch,Route} from 'react-router-dom';
import '../App.css';
import Dashboard from '../views/Dashboard';
import Layout from './index';
import {
    Card,
    CardBody,
    FormGroup,
    Input,
    Label,
    Button,
    Form
} from 'reactstrap';
let dataUser = []
let loggedIn = false
let token 
class LoginPage extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            username: '',
            password: '',
        })
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleChange(e) {
        if (e.target.name === 'username') {
            this.setState({ username: e.target.value })
        } else {
            this.setState({ password: e.target.value })
        }
    }
    handleSubmit(e){
        e.preventDefault()
        const data = this.state        
        console.log(data)
        fetch('http://209.97.160.40:8080/SA061118/auth/token', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
            })
        }).then((response) => {
            return response.json();
        }).then((result) => {
            dataUser = result
            // dataUser['access_token']
            console.log(dataUser)
            if (dataUser['status'] === "success") { // when user success login
                console.log('Login')
                loggedIn = true
                token = dataUser['access_token']
                console.log(token)             
            }else{ // when user fail login
                loggedIn = false
                alert(dataUser['message']) 
            }
        });
        
    }
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <Card className="app-card">
                        <CardBody>
                            <div className="text-center mb-4 mt-3"><h2>Login</h2></div>
                            <Form onSubmit={this.handleSubmit} id="login-form">
                                <FormGroup>
                                    <Label>Username</Label>
                                    <Input name="username" type="text" className="form-control" placeholder="Input username" onChange={this.handleChange} />
                                    <h5>{this.state.username}</h5>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Password</Label>
                                    <Input name="password" type="password" className="form-control" placeholder="Input correct password" onChange={this.handleChange} />
                                    <h5>{this.state.password}</h5>
                                </FormGroup>
                            <div className="mb-3">
                                <Button outline color="info" block type="submit">Login</Button>
                            </div>
                            </Form>
                        </CardBody>
                    </Card>
                </div>
            </div>
        );
    }
}

export default LoginPage;