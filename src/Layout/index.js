import React, { Component } from 'react';
import NavigasiBar from './NavigasiBar';
import SideBar from './SideBar';

class index extends Component {
    render() {
        return (
            <div>
                <NavigasiBar />
                <div
                    style={{
                        position:'relative',
                    }}
                >
                    <SideBar />
                </div>
            </div>
        );
    }
}

export default index;