import React, { Component } from 'react';
import {
    Container,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    Collapse,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
class NavigasiBar extends Component {
    constructor(props) {
        super(props)
        this.toogle = this.toogle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    toogle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <div className="navigasi-bar">
                <Navbar light expand="md" className="text-white color">
                    <Container>
                        <NavbarBrand href="/dashboard" className="text-white">
                            HRMS</NavbarBrand>
                        <NavbarToggler onClick={this.toogle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                    <NavLink href="/#" className="text-white">
                                        <i className="fa fa-sign-out"></i> Logout
                                    </NavLink>
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </Container>
                </Navbar>
            </div>
        );
    }
}

export default NavigasiBar;