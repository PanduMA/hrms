import React from 'react';

const Dashboard = React.lazy(() => import('./views/Dashboard'));
const ListSDM = React.lazy(() => import('./views/SDM/List'));
const HistorisSDM = React.lazy(() => import('./views/SDM/Historis'));
const PyscologySDM = React.lazy(() => import('./views/SDM/Psycology'));
const routes = [
    { path: '/', exact: true, name: 'Home' },
    { path: '/dashboard', name : 'Dashboard', component:Dashboard },
    { path: '/sdm/list', name : 'List SDM', component:ListSDM },
    { path: '/sdm/historis', name : 'Historis SDM', component:HistorisSDM },
    { path: '/sdm/psycology', name : 'Psycology SDM', component:PyscologySDM },
];
export default routes;