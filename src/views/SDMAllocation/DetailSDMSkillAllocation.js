import React, { Component } from 'react';
import {
    Card,
    CardBody,
    CardFooter,
    Label,
    Table,
    Button
} from 'reactstrap';
class DetailSDMSkillAllocation extends Component {
    render() {
        return (
            <div>
                <Card>
                    <CardBody className="px-5 py-5">
                        <Label>SDM Name : </Label>
                        <Label>...</Label>
                        <Table responsive hover striped className="text-center">
                            <thead className="thead-dark">
                                <tr>
                                    <td>Skill Type Name</td>
                                    <td>Skill Name</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>...</td>
                                    <td>...</td>
                                </tr>
                            </tbody>
                        </Table>
                    </CardBody>
                    <CardFooter>
                        <Button outline color="primary"> 
                            Back
                        </Button>
                    </CardFooter>
                </Card>
            </div>
        );
    }
}

export default DetailSDMSkillAllocation;