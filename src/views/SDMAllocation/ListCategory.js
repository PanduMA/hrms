import React, { Component } from 'react';
import {
    Card,
    CardBody,
    FormGroup,
    Input,
    Label,
    Button,
    Row,
    Col,
    Table,
    UncontrolledTooltip
} from 'reactstrap';
class ListCategory extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            categorySkill: '',
            skillName: ''
        })
        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }
    handleClick(e) {
        if (e.target.name === 'reset') {
            this.setState({
                categoryName: ''
            })
        } else {
            //do when click submit
        }
    }
    handleChange(e) {
        this.setState({
            categoryName: e.target.value,
        })
    }
    render() {
        return (
            <div>
                <Card>
                    <CardBody className="px-5 py-5">
                        <Label>Add Category Name</Label>
                        <Row>
                            <Col sm={{ size: 4 }}>
                                <FormGroup>
                                    <Input type="text" placeholder="Category Name" name="categoryName" required onChange={this.handleChange} value={this.state.categoryName} />
                                    <h3>{this.state.categoryName}</h3>
                                </FormGroup>
                            </Col>
                            <Col sm={{ size: 2 }}>
                                <Button type="submit" outline color="info" name="submit" block onClick={this.handleClick} className={this.state.categoryName ? '' : 'disabled'} >
                                    <i className="fa fa-search"></i> Submit
                                </Button>
                            </Col>
                            <Col sm={{ size: 2 }}>
                                <Button type="reset" outline color="danger" name="reset" block onClick={this.handleClick}>
                                    <i className="fa fa-close"></i> Reset
                                </Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                <br />
                <Card>
                    <CardBody className="px-5 py-5">
                        <Table responsive hover striped className="text-center">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Category Id</th>
                                    <th>Category Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tes</td>
                                    <td>Tes</td>
                                    <td>
                                        <Button outline color="primary" className="mr-2" id="edit-skill"><i className="fa fa-edit"></i></Button>
                                        <UncontrolledTooltip placement="top" target="edit-skill">Edit Data Category</UncontrolledTooltip>
                                        <Button outline color="danger" id="delete-skill"><i className="fa fa-trash"></i></Button>
                                        <UncontrolledTooltip placement="top" target="delete-skill">Delete Data Category</UncontrolledTooltip>
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default ListCategory;