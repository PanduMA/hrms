import React, { Component } from 'react';
import {
    Card,
    CardBody,
    FormGroup,
    Input,
    Label,
    Button,
    Row,
    Col,
    Table,
    UncontrolledTooltip
} from 'reactstrap';
class ListSkill extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            categorySkill: '',
            skillName: ''
        })
        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }
    handleClick(e) {
        if (e.target.name === 'reset') {
            this.setState({
                categorySkill: '',
                skillName: ''
            })
        } else {
            //do when click submit
        }
    }
    handleChange(e) {
        if (e.target.name === 'skillName') {
            this.setState({
                skillName: e.target.value,
            })
        }
        else if (e.target.name === 'categorySkill' && e.target.value !== 'prompt') {
            this.setState({
                categorySkill: e.target.value,
            })
        }
    }
    render() {
        return (
            <div>
                <Card>
                    <CardBody className="px-5 py-5">
                        <Label>Add Skill Name</Label>
                        <Row>
                            <Col sm={{ size: 4 }}>
                                <FormGroup>
                                    <Input type="select" name="categorySkill" onChange={this.handleChange} value={this.state.categorySkill} >
                                        <option value="prompt">Please Choose...</option>
                                        <option>Contoh</option>
                                    </Input>
                                </FormGroup>
                                <h3>{this.state.categorySkill}</h3>
                            </Col>
                            <Col sm={{ size: 4 }}>
                                <FormGroup>
                                    <Input type="text" placeholder="Enter Skill Name" name="skillName" required onChange={this.handleChange} value={this.state.skillName} />
                                    <h3>{this.state.skillName}</h3>
                                </FormGroup>
                            </Col>
                            <Col sm={{ size: 2 }}>
                                <Button type="submit" outline color="primary" name="submit" block onClick={this.handleClick} className={this.state.skillName && this.state.categorySkill ? '' : 'disabled'} >
                                    <i className="fa fa-search"></i> Submit
                                </Button>
                            </Col>
                            <Col sm={{ size: 2 }}>
                                <Button type="reset" outline color="danger" name="reset" block onClick={this.handleClick}>
                                    <i className="fa fa-close"></i> Reset
                                </Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                <br />
                <Card>
                    <CardBody className="px-5 py-5">
                        <Table responsive hover striped className="text-center">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Skill Id</th>
                                    <th>Category Name</th>
                                    <th>Skill Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tes</td>
                                    <td>Tes</td>
                                    <td>Tes</td>
                                    <td>
                                        <Button outline color="primary" className="mr-2" id="edit-skill"><i className="fa fa-edit"></i></Button>
                                        <UncontrolledTooltip placement="top" target="edit-skill">Edit Data Skill</UncontrolledTooltip>
                                        <Button outline color="danger" id="delete-skill"><i className="fa fa-trash"></i></Button>
                                        <UncontrolledTooltip placement="top" target="delete-skill">Delete Data Skill</UncontrolledTooltip>
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default ListSkill;