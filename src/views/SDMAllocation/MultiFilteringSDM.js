import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    FormGroup,
    Button,
    Input,
    CustomInput,
} from 'reactstrap';
class MultiFilteringSDM extends Component {
    render() {
        return (
            <div>
                <Card>
                    <CardBody className="px-5 py-5">
                        <FormGroup row>
                            <Col sm={{size:7,offset:2}}>
                                <Input type="text" placeholder="Enter SDM Name" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col sm={{size:3,offset:2}}>
                                <CustomInput type="checkbox" id="exampleCustomInline" label="Combine Filtering Mode" inline />                        
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col sm={{size:2,offset:2}}>
                                <Input type="select" name="category">
                                    <option value="prompt">Choose Category</option>
                                </Input>
                            </Col>
                            <Col sm={{size:2}}>
                                <Input type="select" name="skill">
                                    <option value="prompt">Choose Skill</option>
                                </Input>
                            </Col>
                            <Col sm={{size:2}}>
                                <Input type="number" placeholder="Value" min="1" max="10"/>
                            </Col>
                            <Col sm={{size:1}}>
                                <Button outline color="primary">
                                    <i className="fa fa-plus"></i>
                                </Button>
                            </Col>
                        </FormGroup>
                        <Row>
                            <Col sm={{size:2,offset:3}}>
                                <Button outline color="primary" block>
                                    <i className="fa fa-search"></i> Filter
                                </Button>
                            </Col>
                            <Col sm={{size:2}}>
                                <Button outline color="danger" block>
                                    <i className="fa fa-close"></i> Clear
                                </Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default MultiFilteringSDM;