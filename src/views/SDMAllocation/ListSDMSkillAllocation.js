import React, { Component } from 'react';
import { Link} from 'react-router-dom';
import {
    Row,
    Col,
    Card,
    CardBody,
    Table,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Button,
    UncontrolledTooltip,
    FormGroup,
} from 'reactstrap';
import Add from '../SDMAllocation/AddSDMSkillAllocation';
class ListSDMSkillAllocation extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            search: '',
            searchBy: ''
        })
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        if (e.target.name === 'searchby' && e.target.value !== 'prompt') {
            this.setState({
                searchBy: e.target.value,
            })
        } else {
            this.setState({
                search: e.target.value
            })
        }
    }
    handleKeyDown(e) {
        if (e.keyCode === 13) {
            this.setState({
                search: e.target.value
            })
        }
    }
    handleClick(e) {
        if (e.target.name === 'reset') {
            this.setState({
                search: '',
                searchBy: ''
            })
        }else{
            alert('oi');
        }
    }
    render() {
        return (
            <div>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card>
                            <CardBody className="px-5 py-5">
                                <FormGroup row>
                                    <Label sm={{ size: 1 }} >Search by</Label>
                                    <Col sm={{ size: 3 }}>
                                        <Input type="select" name="searchby" value={this.state.searchBy} onChange={this.handleChange}>
                                            <option value="prompt">Please Choose...</option>
                                            <option>SDM Name</option>
                                            <option>SKill</option>
                                        </Input>
                                        <h3>{this.state.searchBy}</h3>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <InputGroup>
                                            <Input type="text" onKeyDown={this.handleKeyDown} placeholder="Enter keyword" value={this.state.search} onChange={this.handleChange} />
                                            <InputGroupAddon addonType="append">
                                                <InputGroupText>
                                                    <i className="fa fa-search"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                        </InputGroup>
                                    </Col>
                                    <h3>{this.state.search}</h3>
                                    <Col sm={{ size: 2 }}>
                                        <Button type="reset" outline color="danger" name="reset" block onClick={this.handleClick}>
                                            <i className="fa fa-close"></i> Reset
                                        </Button>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Link to="/sdmallocation/sdmskill/add">
                                        <Button type="submit" outline color="primary" block>
                                            <i className="fa fa-fw fa-plus" />
                                            Add SDM Skill
                                        </Button>
                                        </Link>
                                    </Col>
                                </FormGroup>
                                <br />
                                <Table responsive hover striped className="text-center">
                                    <thead className="thead-dark">
                                        <tr>
                                            <th>No</th>
                                            <th>NIK</th>
                                            <th>SDM Name</th>
                                            <th>End Date Project</th>
                                            <th>Notification Contract</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <Button outline color="primary" id="view-sdm" className="mr-2"><i className="fa fa-eye"></i></Button>
                                                <UncontrolledTooltip placement="top" target="view-sdm">View Detail SDM Allocation</UncontrolledTooltip>
                                                <Button outline color="primary" className="mr-2" id="edit-skill"><i className="fa fa-edit"></i></Button>
                                                <UncontrolledTooltip placement="top" target="edit-skill">Edit Data Skill Allocation</UncontrolledTooltip>
                                                <Button outline color="danger" id="delete-skill"><i className="fa fa-trash"></i></Button>
                                                <UncontrolledTooltip placement="top" target="delete-skill">Delete Data Skill Allocation</UncontrolledTooltip>
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default ListSDMSkillAllocation;