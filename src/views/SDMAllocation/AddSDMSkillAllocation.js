import React, { Component } from 'react';
import {
    Card,
    CardBody,
    FormGroup,
    Label,
    Col,
    Input,
    Row,
    Button
} from 'reactstrap';
class AddSDMSkillAllocation extends Component {
    render() {
        return (
            <div>
                <Card>
                    <CardBody className="px-5 py-5">
                        <FormGroup row>
                            <Label sm={{size:2,offset:2}} className="text-right">SDM Name</Label>
                            <Col sm={{size:4}}>
                                <Input type="text" placeholder="Enter SDM Name" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{size:2,offset:2}} className="text-right">NIK</Label>
                            <Label sm={{size:4}}>...</Label>
                        </FormGroup>
                        <FormGroup row>
                            {/* <Label sm={{size:2}} className="text-right">Category Skill</Label> */}
                            <Col sm={{size:3,offset:1}}>
                                <Input type="select">
                                    <option>Choose Category Skill</option>
                                </Input>
                            </Col>
                            {/* <Label sm={{size:1}}>List Skill</Label> */}
                            <Col sm={{size:3}}>
                                <Input type="select">
                                    <option>Choose List Skill</option>
                                </Input>
                            </Col>
                            <Col sm={{size:3}}>
                                <Input type="number" min="1" max="10" placeholder="Value" />
                            </Col>
                            <Col sm={{size:1}}>
                                <Button outline color="primary">
                                    <i className="fa fa-plus"></i>
                                </Button>
                            </Col>
                        </FormGroup>
                        <Row>
                            <Col sm={{size:3,offset:3}}>
                                <Button outline color="primary" block>Save</Button>
                            </Col>
                            <Col sm={{size:3}}>
                                <Button outline color="danger" block>Back</Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default AddSDMSkillAllocation;