import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    CardFooter,
    Table,
    FormGroup,
    Label,
    Input,
    Button,
    UncontrolledTooltip
} from 'reactstrap';
class List extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            level: '',
            name: '',
            endDate: '',
            nameButton: '',
        });
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        if (e.target.name === 'level' && e.target.value !== 'prompt') {
            this.setState({
                level: e.target.value,
            })
        }
        else if (e.target.name === 'name') {
            this.setState({
                name: e.target.value,
            })
        } else if (e.target.name === 'endDate') {
            this.setState({
                endDate: e.target.value
            })
        }

    }
    handleClick(e) {
        e.preventDefault();
        this.setState({
            nameButton: e.target.name
        })
        if (e.target.name === 'clear') { // do clear click button
            this.setState({
                level: '',
                name: '',
                endDate: '',
            })
        }
        else { // do filter click button

        }
    }

    render() {
        return (
            <div>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card>
                            <CardBody className="px-5 py-5">
                                <Row>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>Level : </Label>
                                            <Input type="select" name="level" value={this.state.level} onChange={this.handleChange}>
                                                <option value="prompt">Please Choose...</option>
                                                <option value="junior">Junior</option>
                                            </Input>
                                            <h3>{this.state.level}</h3>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>Name : </Label>
                                            <Input type="text" placeholder="Enter Name SDM" onChange={this.handleChange} value={this.state.name} name="name" />
                                            <h3>{this.state.name}</h3>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>End Date : </Label>
                                            <Input type="date" onChange={this.handleChange} value={this.state.endDate} name="endDate" />
                                            <h3>{this.state.endDate}</h3>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={{ size: 2 }}>
                                        <Button outline color="primary" block onClick={this.handleClick} name="filter">
                                            <i className="fa fa-fw fa-search" />
                                            Filter
                                            </Button>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Button outline color="danger" block onClick={this.handleClick} name="clear">
                                            <i className="fa fa-fw fa-close" />
                                            Clear
                                            </Button>
                                    </Col>
                                </Row>
                            </CardBody>
                            <CardFooter>
                                <Button outline color="primary" className="float-right">
                                    <i className="fa fa-fw fa-plus" />
                                    Input SDM</Button>
                            </CardFooter>
                        </Card>
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card>
                            <CardBody className="px-5 py-5">
                                <Table responsive hover striped className="text-center">
                                    <thead className="thead-dark">
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>NIK</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                            <th>Notification</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Tes</td>
                                            <td>Tes</td>
                                            <td>Tes</td>
                                            <td>Tes</td>
                                            <td>Tes</td>
                                            <td>Tes</td>
                                            <td>
                                                <Button outline color="primary" className="mr-2" id="print-cv">
                                                    <i className="fa fa-print"></i>
                                                </Button>
                                                <UncontrolledTooltip placement="top" target="print-cv">
                                                    Print CV
                                                </UncontrolledTooltip>
                                                <Button outline color="primary" className="mr-2" id="edit-sdm">
                                                    <i className="fa fa-edit"></i>
                                                </Button>
                                                <UncontrolledTooltip placement="top" target="edit-sdm">
                                                    Edit Data SDM
                                                </UncontrolledTooltip>
                                                <Button outline color="primary" id="view-sdm">
                                                    <i className="fa fa-eye"></i>
                                                </Button>
                                                <UncontrolledTooltip placement="top" target="view-sdm">
                                                    View Detail SDM
                                                </UncontrolledTooltip>
                                            </td>
                                            <td>
                                                <i className="fa fa-warning fa-2x" id="notification"></i>
                                                <UncontrolledTooltip placement="top" target="notification">
                                                    1 Month Left
                                                </UncontrolledTooltip>
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default List;