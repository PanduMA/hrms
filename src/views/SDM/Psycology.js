import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    Table,
    FormGroup,
    Input,
    Label,
    Button,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    UncontrolledTooltip
} from 'reactstrap';
class Psycology extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            client: '',
            searchBy: '',
            search: ''
        })
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
    }
    handleChange(e) {
        if (e.target.name === 'client' && e.target.value !== 'prompt') {
            this.setState({
                client: e.target.value
            })
        } else if (e.target.name === 'searchby' && e.target.value !== 'prompt') {
            this.setState({
                searchBy: e.target.value
            })
        }else{
            this.setState({
                search:e.target.value
            })
        }
    }
    handleClick() {
        this.setState({
            client: '',
            searchBy:'',
            search:''
        })
    }
    handleKeyDown(e) {
        if (e.keyCode === 13) {
            this.setState({
                search: e.target.value
            })
        }
    }
    render() {
        return (
            <div>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card>
                            <CardBody className="px-5 py-5">
                                <FormGroup row>
                                    <Label sm={{ size: 1 }} >Client</Label>
                                    <Col sm={{ size: 4 }}>
                                        <Input type="select" name="client" value={this.state.client} onChange={this.handleChange}>
                                            <option value="prompt" >Please Choose...</option>
                                            <option>contoh</option>
                                        </Input>
                                        <h3>{this.state.client}</h3>
                                    </Col>
                                </FormGroup>
                            </CardBody>
                        </Card>
                        <br />
                        <Card>
                            <CardBody className="px-5 py-5">
                                <FormGroup row>
                                    <Label sm={{ size: 1 }} >Search by</Label>
                                    <Col sm={{ size: 3 }}>
                                        <Input type="select" name="searchby" value={this.state.searchBy} onChange={this.handleChange}>
                                            <option value="prompt">Please Choose...</option>
                                            <option>SDM Name</option>
                                        </Input>
                                        <h3>{this.state.searchBy}</h3>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <InputGroup>
                                            <Input type="text" placeholder="Enter Keyword" onKeyDown={this.handleKeyDown} onChange={this.handleChange} value={this.state.search} name="search" />
                                            <InputGroupAddon addonType="append">
                                                <InputGroupText>
                                                    <i className="fa fa-search"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                        </InputGroup>
                                    </Col>
                                    <h3>{this.state.search}</h3>
                                    <Col sm={{ size: 2 }}>
                                        <Button type="reset" outline color="danger" block onClick={this.handleClick}>
                                            <i className="fa fa-fw fa-close" />
                                            Reset
                                        </Button>
                                    </Col>
                                    </FormGroup>
                                <br />
                                <Table responsive hover striped className="text-center">
                                    <thead className="thead-dark">
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Condition</th>
                                            <th>Reason</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <Button outline color="primary" id="edit-sdm"><i className="fa fa-edit"></i></Button>
                                                <UncontrolledTooltip placement="top" target="edit-sdm">Edit Psycology SDM</UncontrolledTooltip>
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Psycology;