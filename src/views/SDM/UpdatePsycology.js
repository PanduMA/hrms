import React, { Component } from 'react';
import {
    Card,
    CardBody,
    FormGroup,
    Label,
    Col,
    Input,
    Row,
    Button
} from 'reactstrap';
class UpdatePsycology extends Component {
    render() {
        return (
            <div>
                <Card>
                    <CardBody className="px-5 py-5">
                        <FormGroup row>
                            <Label sm={{size:3,offset:1}} className="text-right">SDM Name</Label>
                            <Label sm={{size:5}}>...</Label>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{size:3,offset:1}} className="text-right">Condition</Label>
                            <Col sm={{size:5}}>
                                <Input type="select">
                                    <option>Sad</option>
                                </Input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{size:3,offset:1}} className="text-right">Description</Label>
                            <Col sm={{size:5}}>
                                <Input type="textarea" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{size:3,offset:1}} className="text-right">Date</Label>
                            <Col sm={{size:5}}>
                                <Input type="date" />
                            </Col>
                        </FormGroup>
                        <Row>
                        <Col sm={{size:3,offset:3}}>
                            <Button outline color="primary" block>Update</Button>
                        </Col>
                        <Col sm={{size:3}}>
                            <Button outline color="danger" block>Back</Button>
                        </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default UpdatePsycology;