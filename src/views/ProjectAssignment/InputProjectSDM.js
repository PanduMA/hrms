import React, { Component } from 'react';
import {
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Label,
    Input,
    Row,
    Col,
    Button
} from 'reactstrap';
class InputProjectSDM extends Component {
    render() {
        return (
            <div>
                <Card>
                    <CardHeader>
                        <h4>Add Project SDM</h4>
                    </CardHeader>
                    <CardBody className="px-5">
                        <FormGroup row>
                            <Label sm={{ size: 2 }} className="text-right">
                                <i className="text-danger">*</i>SDM Name :
                            </Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="sdmName" placeholder="Enter SDM Name" required />
                            </Col>
                            <Label sm={{ size: 2 }} className="text-right">
                                <i className="text-danger">*</i>Role Project :
                            </Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="roleProject" placeholder="Enter Role Project" required />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{ size: 2 }} className="text-right">NIK :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="nik" placeholder="NIK Generated" readOnly />
                            </Col>
                            <Label sm={{ size: 2 }} className="text-right">App Type :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="appType" placeholder="Enter App Type" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{ size: 2 }} className="text-right">
                                <i className="text-danger">*</i>Project Name :
                            </Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="projectName" placeholder="Enter Project Name" required />
                            </Col>
                            <Label sm={{ size: 2 }} className="text-right">Dev Language :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="devLang" placeholder="Enter Dev Language" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{ size: 2 }} className="text-right">Customer :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="customer" placeholder="Enter Customer" />
                            </Col>
                            <Label sm={{ size: 2 }} className="text-right">Server OS :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="serverOS" placeholder="Enter Server OS" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{ size: 2 }} className="text-right">
                                <i className="text-danger">*</i>Project Site :
                            </Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="projectSite" placeholder="Enter Project Site" required />
                            </Col>
                            <Label sm={{ size: 2 }} className="text-right">Framework :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="framework" placeholder="Enter Framework" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{ size: 2 }} className="text-right">
                                <i className="text-danger">*</i>Period Start :
                            </Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="date" name="periodStart" required />
                            </Col>
                            <Label sm={{ size: 2 }} className="text-right">Database :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="database" placeholder="Enter Database" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{ size: 2 }} className="text-right">
                                <i className="text-danger">*</i>Period End :
                            </Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="date" name="periodEnd" required />
                            </Col>
                            <Label sm={{ size: 2 }} className="text-right">Dev Tools :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="devTools" placeholder="Enter Dev Tools" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{ size: 2 }} className="text-right">
                                <i className="text-danger">*</i>Project Desc :
                            </Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="textarea" name="projectDesc" placeholder="Enter Project Desc" required />
                            </Col>
                            <Label sm={{ size: 2 }} className="text-right">Other Info :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="textarea" name="otherInfo" placeholder="Enter Other Info" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label sm={{ size: 2 }} className="text-right">App Server :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="text" name="appServer" placeholder="Enter App Server"/>
                            </Col>
                            <Label sm={{ size: 2 }} className="text-right">Technical Info :</Label>
                            <Col sm={{ size: 4 }}>
                                <Input type="textarea" name="technicalInfo" placeholder="Enter Technical Info" />
                            </Col>
                        </FormGroup>
                        <Row>
                            <Col sm={{size:3,offset:5}}>
                                <Button type="submit" outline color="primary" block>
                                    Submit
                                </Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default InputProjectSDM;