import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    Table,
    FormGroup,
    Label,
    Input,
    Button,
    UncontrolledTooltip
} from 'reactstrap';
class ListDetailProjectSDM extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            project: '',
            name: '',
            endDate: '',
            nameButton: '',
        });
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        if (e.target.name === 'project' ) {
            this.setState({
                project: e.target.value,
            })
        }
        else if (e.target.name === 'name') {
            this.setState({
                name: e.target.value,
            })
        } else if (e.target.name === 'endDate') {
            this.setState({
                endDate: e.target.value
            })
        }

    }
    handleClick(e) {
        e.preventDefault();
        this.setState({
            nameButton: e.target.name
        })
        if (e.target.name === 'clear') { // do clear click button
            this.setState({
                project: '',
                name: '',
                endDate: '',
            })
        }
        else { // do filter click button

        }
    }
    render() {
        return (
            <div>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card>
                            <CardBody className="px-5 py-5">
                                <Row>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>Name : </Label>
                                            <Input type="text" placeholder="Enter Name SDM" onChange={this.handleChange} value={this.state.name} name="name" />
                                            <h3>{this.state.name}</h3>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>Project Name : </Label>
                                            <Input type="text" placeholder="Enter Project Name" name="project" value={this.state.project} onChange={this.handleChange} />
                                            <h3>{this.state.project}</h3>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                        <FormGroup>
                                            <Label>End Date : </Label>
                                            <Input type="date" onChange={this.handleChange} value={this.state.endDate} name="endDate" />
                                            <h3>{this.state.endDate}</h3>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={{ size: 2 }}>
                                        <Button outline color="primary" block onClick={this.handleClick} name="filter">
                                            <i className="fa fa-fw fa-search" />
                                            Filter
                                            </Button>
                                    </Col>
                                    <Col sm={{ size: 2 }}>
                                        <Button outline color="danger" block onClick={this.handleClick} name="clear">
                                            <i className="fa fa-fw fa-close" />
                                            Clear
                                            </Button>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card>
                            <CardBody className="px-5 py-5">
                                <Table responsive hover striped className="text-center">
                                    <thead className="thead-dark">
                                        <tr>
                                            <th>No</th>
                                            <th>Name SDM</th>
                                            <th>Project Name</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Notification Project</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Tes</td>
                                            <td>Tes</td>
                                            <td>Tes</td>
                                            <td>Tes</td>
                                            <td>Tes</td>
                                            <td>
                                                <i className="fa fa-2x fa-warning" id="notification"></i>
                                                <UncontrolledTooltip placement="top" target="notification">
                                                    Non aktif
                                                </UncontrolledTooltip>
                                            </td>
                                            <td>
                                                <Button outline color="primary" id="view-sdm" className="mr-2">
                                                    <i className="fa fa-eye"></i>
                                                </Button>
                                                <UncontrolledTooltip placement="top" target="view-sdm">
                                                    View Detail SDM
                                                </UncontrolledTooltip>
                                                <Button outline color="primary" className="mr-2" id="edit-sdm">
                                                    <i className="fa fa-edit"></i>
                                                </Button>
                                                <UncontrolledTooltip placement="top" target="edit-sdm">
                                                    Edit Data SDM
                                                </UncontrolledTooltip>
                                                <Button outline color="danger" id="delete-sdm">
                                                    <i className="fa fa-trash"></i>
                                                </Button>
                                                <UncontrolledTooltip placement="top" target="delete-sdm">
                                                    Delete Data SDM
                                                </UncontrolledTooltip>
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default ListDetailProjectSDM;