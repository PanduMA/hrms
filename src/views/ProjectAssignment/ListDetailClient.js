import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    Table,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Button,
    UncontrolledTooltip,
    FormGroup
} from 'reactstrap';
class ListDetailClient extends Component {
    constructor(props){
        super(props)
        this.state = ({
            search:'',
            searchBy:''
        })
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    handleChange(e) {
        if (e.target.name === 'searchby' && e.target.value !== 'prompt') {
            this.setState({
                searchBy: e.target.value,
            })
        }else{
            this.setState({
                search:e.target.value
            })
        }
    }
    handleKeyDown(e){ //handle when user press enter
        if (e.keyCode === 13) {
            this.setState({
                search:e.target.value
            })
        }
    }
    handleClick() {
        this.setState({
            searchBy: '',
            search:''
        })
    }
    render() {
        return (
            <div>
                <Row>
                    <Col sm={{ size: 12 }}>
                        <Card>
                            <CardBody className="px-5 py-5">
                                <Row>
                                    <Label sm={{ size: 1}} >Search by</Label>
                                    <Col sm={{ size: 3 }}>
                                    <FormGroup>
                                        <Input type="select" name="searchby" value={this.state.searchBy} onChange={this.handleChange}>
                                            <option value="prompt">Please Choose...</option>
                                            <option>Client Name</option>
                                        </Input>
                                        <h3>{this.state.searchBy}</h3>
                                        </FormGroup>
                                    </Col>
                                    <Col sm={{ size: 4 }}>
                                    <FormGroup>
                                        <InputGroup>
                                            <Input type="text" onKeyDown={this.handleKeyDown} placeholder="Enter Keyword" value={this.state.search} onChange={this.handleChange}/>
                                            <InputGroupAddon addonType="append">
                                                <InputGroupText>
                                                    <i className="fa fa-search"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                        </InputGroup>
                                        </FormGroup>
                                    </Col>
                                    <h3>{this.state.search}</h3>
                                    <Col sm={{ size: 2 }}>
                                        <Button type="reset" outline color="danger" block onClick={this.handleClick}>
                                            <i className="fa fa-fw fa-close" />
                                            Reset
                                        </Button>
                                    </Col>
                                </Row>
                                <br />
                                <Table responsive hover striped className="text-center">
                                    <thead className="thead-dark">
                                        <tr>
                                            <th>No</th>
                                            <th>Client Name</th>
                                            <th>PIC Handler</th>
                                            <th>Contact Person</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <Button outline color="primary" className="mr-2" id="edit-sdm">
                                                    <i className="fa fa-edit"></i>
                                                </Button>
                                                <UncontrolledTooltip placement="top" target="edit-sdm">
                                                    Edit Data Client
                                                </UncontrolledTooltip>
                                                <Button outline color="danger" id="delete-sdm">
                                                    <i className="fa fa-trash"></i>
                                                </Button>
                                                <UncontrolledTooltip placement="top" target="delete-sdm">
                                                    Delete Data Client
                                                </UncontrolledTooltip>
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default ListDetailClient;