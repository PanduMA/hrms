import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    Table,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Button,
    UncontrolledTooltip,
    FormGroup
} from 'reactstrap';
class HiringSDM extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            search: '',
            client: '',
            searchBy:''
        })
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    handleChange(e) {
        if (e.target.name === 'client' && e.target.value !== 'prompt') {
            this.setState({
                client: e.target.value,
            })
        } else if(e.target.name ==='searchby'){
            this.setState({
                searchBy: e.target.value
            })
        }else{
            this.setState({
                search: e.target.value
            })
        }
    }
    handleKeyDown(e) { //handle when user press enter
        if (e.keyCode === 13) {
            this.setState({
                search: e.target.value
            })
        }
    }
    handleClick(e) {
        if (e.target.type === 'reset') {
            this.setState({
                client:'',
            })    
        }else{

        }
    }
    render() {
        return (
            <div>
                <Card>
                    <CardBody className="px-5 py-5">
                        <Row>
                            <Label sm={{ size: 1 }}>Client:</Label>
                            <Col sm={{ size: 3 }}>
                                <FormGroup>
                                    <Input type="select" name="client" value={this.state.client} onChange={this.handleChange}>
                                        <option value="prompt">Please Choose...</option>
                                        <option>Name</option>
                                    </Input>
                                    <h3>{this.state.client}</h3>
                                </FormGroup>
                            </Col>
                            <Col sm={{ size: 4 }}>
                                <Label>PIC Client:</Label>
                                <Label> ..</Label>
                            </Col>
                            <Col sm={{ size: 4 }}>
                                <Label>Client Contact:</Label>
                                <Label> ..</Label>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={{ size: 2,offset:1}}>
                                <Button type="submit" outline color="primary" block onClick={this.handleClick}>
                                    <i className="fa fa-fw fa-plus" />
                                    Hire SDM
                                </Button>
                            </Col>
                            <Col sm={{ size: 2}}>
                                <Button type="reset" outline color="danger" block onClick={this.handleClick}>
                                    <i className="fa fa-fw fa-close" />
                                    Reset
                                </Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                <br />
                <Card>
                    <CardBody className="px-5 py-5">
                        <Row>
                            <Label sm={{ size: 1 }} >Search by</Label>
                            <Col sm={{ size: 3 }}>
                                <FormGroup>
                                    <Input type="select" name="searchby" value={this.state.searchBy} onChange={this.handleChange}>
                                        <option value="prompt">Please Choose...</option>
                                        <option>Name</option>
                                        <option>Status</option>
                                    </Input>
                                    <h3>{this.state.searchBy}</h3>
                                </FormGroup>
                            </Col>
                            <Col sm={{ size: 4 }}>
                                <FormGroup>
                                    <InputGroup>
                                        <Input type="text" onKeyDown={this.handleKeyDown} placeholder="Enter Keyword" value={this.state.search} onChange={this.handleChange} />
                                        <InputGroupAddon addonType="append">
                                            <InputGroupText>
                                                <i className="fa fa-search"></i>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                    </InputGroup>
                                </FormGroup>
                            </Col>
                            <h3>{this.state.search}</h3>
                        </Row>
                        <br />
                        <Table responsive hover striped className="text-center">
                            <thead className="thead-dark">
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <Button outline color="primary" className="mr-2" id="edit-sdm">
                                            <i className="fa fa-edit"></i>
                                        </Button>
                                        <UncontrolledTooltip placement="top" target="edit-sdm">
                                            Edit Data SDM
                                        </UncontrolledTooltip>
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default HiringSDM;