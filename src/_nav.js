export default {
    items : [
        {
            name : 'Dashboard',
            url : '/dashboard',
            icon : 'icon-home',
        },
        {
            name : 'SDM',
            url : '/sdm',
            icon : 'icon-people',
            children : [
                {
                    name : 'List SDM',
                    url : '/sdm/list',
                    icon : 'icon-list',
                },
                {
                    name : 'Historis SDM',
                    url : '/sdm/historis',
                    icon : 'icon-sort-ascending',
                },
                {
                    name : 'List Psycology',
                    url : '/sdm/psycology',
                    icon : 'icon-shield'
                },
            ],
        },
    ],
};